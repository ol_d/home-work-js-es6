// #1
function viselitsia() {
    const words = ['ШКАФ', 'КАВКАЗ', 'ГАЗ', 'БЕНЗИН', 'БРАТ', 'ВЕТЕРАН', 'ВОИН', 'УЛЬТРАМИКРОСКОП', 'ПОБЕДИТЕЛЬ', 'КОМПАС'];

    let targetWordArr = [words[Math.floor(Math.random() * (words.length))].toLowerCase().split('')];
    let answArr = [];
    for(let i = 0; i < targetWordArr[0].length; i++) {
        answArr[i] = '_';
    }

    askUser();

    function askUser() {
        let letter = prompt(`вы угадываете слово: ${answArr.join(' ')}, введите букву`);
        for(let i = 0; i < answArr.length; i++){
            if(letter === targetWordArr[0][i]){
                answArr[i] =targetWordArr[0][i];
            }
        }
        if(answArr.includes('_')){
            askUser()
        }else{
            alert(`Вы угадали слово ${answArr.join('')}`);
        }
    }
}

viselitsia();



// #2
function createNewUser(){
    let firstName = prompt('name?');
    let lastName = prompt('lastname?');
    class NewUser {
        constructor(){
            this.firstName = firstName;
            this.lastName = lastName;
            this.birthday;
        }
        getLogin(){
            let login = (this.firstName[0].toString() + this.lastName.toString()).toLowerCase();
            return login;
        }
        askUserBirthDay() {
            this.birthday = prompt('date of birth "dd.mm.yyyy"?');
        }
        getAge() {
            if(this.birthday == undefined){
                this.askUserBirthDay();
            }
            let answerRightFormat = `${this.birthday.slice(6)}-${this.birthday.slice(3,5)}-${this.birthday.slice(0,2)}`;
            let userAge = Math.floor((new Date() - new Date(answerRightFormat)) / (1000 * 60 * 60 * 24 * 30 * 12));
            return userAge;
        }
        getPassword() {
            if(this.birthday == undefined){
                this.askUserBirthDay();
            }
            let answ = this.firstName[0].toUpperCase() + this.lastName.toLowerCase()+this.birthday.slice(6);
            return answ;
        }
    }
    return new NewUser();
}

let user1 = createNewUser();
console.log(user1);
console.log(user1.getLogin());
console.log(user1.getAge());
console.log(user1.getPassword());




//  #3
function filterBy(arr, type){
    arr.forEach((element, i, arr) => {
        if (typeof(element) === type){
            arr.splice(i, 1);
        }
    });
    
}
